﻿using Amazon;
using Amazon.EC2.Model;
using AwsRdpGenerator.Business;
using AwsRdpGenerator.Model;
using AwsRdpGenerator.Web.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AwsRdpGenerator.Web.Core
{
    public class AwsRdpService
    {
        EC2 ec2;

        public AwsRdpService()
        {
            ec2 = new EC2();
        }

        public static InstanceKeyNameToDomainMap DefaultKeyToDomainMapping
        {
            get
            {
                InstanceKeyNameToDomainMap domainMap = new InstanceKeyNameToDomainMap();
                domainMap.AddOrAmendMapping("gen3-uat", "g3.dom");
                domainMap.AddOrAmendMapping("gen3_aws", "g3prod.dom");
                return domainMap;
            }
        }

        public List<Instance> SydneyInstances
        {
            get
            {
                return ec2.GetEC2InstancesByEndpoint(RegionEndpoint.APSoutheast2);
            }
        }

        public string GetCurrentWindowsUser()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            userName = userName.Substring(userName.IndexOf(@"\") + 1);
            return userName;
        }

        public string GenerateRdpBaseFolderPath()
        {
            string baseFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "rdp_files");
            string userName = GetCurrentWindowsUser();
            string folderPath = Path.Combine(baseFolder, userName);
            return folderPath;
        }

        public string GenerateRdpFullUrlPath(string baseUrl, string fileName)
        {
            string userName = GetCurrentWindowsUser(); ;
            string url = baseUrl + "rdp_files/" + userName + "/";
            return url + fileName;
        }

        public List<RDPClient> GenerateRdpFiles(List<Instance> instances, string baseUrl)
        {
            string userName = GetCurrentWindowsUser();
            RDPProperties props = PropertiesPersister.ReadFromDisk();
            List<RDPClient> filesGenerated = RDP.GenerateRDPFilesForEC2Instances(instances, DefaultKeyToDomainMapping, props);
            string folderPath = GenerateRdpBaseFolderPath();
            RDP.WriteAllRDPFilesToDisk(filesGenerated, folderPath);
            foreach (var item in filesGenerated)
            {
                item.UrlPath = GenerateRdpFullUrlPath(baseUrl, item.FileName);
            }
            return filesGenerated;
        }

        public List<Instance> GetInstancesById(string instanceId)
        {
            return ec2.GetInstancesByInstanceId(RegionEndpoint.APSoutheast2, instanceId);
        }
        
    }
}