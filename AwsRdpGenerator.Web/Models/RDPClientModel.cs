﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AwsRdpGenerator.Web.Models
{
    public class RDPClientModel
    {
        public string URL { set; get; }
        public string Name { set; get; }
    }
}