﻿using Amazon;
using Amazon.EC2.Model;
using AwsRdpGenerator.Business;
using AwsRdpGenerator.Model;
using AwsRdpGenerator.Web.Core;
using AwsRdpGenerator.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AwsRdpGenerator.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            AwsRdpService awsrdp = new AwsRdpService();
            ViewBag.UserName = User.Identity.Name;
            ViewBag.RdpProperties = PropertiesPersister.ReadFromDisk();
            List<Instance> instances = awsrdp.SydneyInstances.Where(x => x.Platform != null).ToList();
            instances = instances.OrderBy(x => x.Tags.FirstOrDefault(y => y.Key == "Name").Value).ToList();
            return View(instances);
        }

        [HttpPost]
        public ActionResult _GenerateRdp(string instanceid)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            AwsRdpService awsrdp = new AwsRdpService();
            List<Instance> instance = awsrdp.GetInstancesById(instanceid);
            RDPClient client = awsrdp.GenerateRdpFiles(instance, baseUrl).FirstOrDefault();

            return Redirect(client.UrlPath);
        }

        [HttpPost]
        public ActionResult _EditRdpProperties(RDPProperties properties)
        {
            PropertiesPersister.SaveToDisk(properties);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult _GetConsoleImageBase64(string instanceId)
        {
            EC2 ec2 = new EC2();
            string base64Image = ec2.GetConsoleScreenshotByInstanceId(RegionEndpoint.APSoutheast2, instanceId);
            return Content(base64Image, "text/html");
        }

    }
}