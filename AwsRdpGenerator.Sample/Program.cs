﻿using Amazon;
using Amazon.EC2.Model;
using AwsRdpGenerator.Business;
using AwsRdpGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AwsRdpGenerator.Sample
{
    class Program
    {
        static void Main(string[] args)
        {          

            Console.WriteLine("Querying list of Instances from AWS...");
            EC2 ec2 = new EC2();
            ec2.GetConsoleScreenshotByInstanceId(RegionEndpoint.APSoutheast2, "i-0090049a1f273df6e");
            List<RegionEndpoint> utilisedRegionEndpoints = new List<RegionEndpoint>()
            {
                RegionEndpoint.APSoutheast2
            };
            List<Instance> ec2instances = ec2.GetAllEC2Instances(utilisedRegionEndpoints);
            foreach (Instance instance in ec2instances)
            {
                string msg = string.Format("Index:{0} TagName:{1} IP:{2} EIP:{3} Instance:{4} KeyName:{5} Status:{6}",
                    ec2instances.IndexOf(instance),
                    instance.Tags.Where(x => x.Key == "Name").FirstOrDefault().Value, 
                    instance.PrivateIpAddress, 
                    instance.PublicIpAddress, 
                    instance.InstanceId, 
                    instance.KeyName, 
                    instance.State.Name);
                Console.WriteLine(msg);
            }
            InstanceKeyNameToDomainMap domainMap = new InstanceKeyNameToDomainMap();
            domainMap.AddOrAmendMapping("gen3-uat", "g3.dom");
            domainMap.AddOrAmendMapping("gen3_aws", "g3prod.dom");
            Console.WriteLine("Generating RDP Clients...");
            List<RDPClient> filesGenerated = RDP.GenerateRDPFilesForEC2Instances(ec2instances, domainMap, "Administrator");
            RDP.WriteAllRDPFilesToDisk(filesGenerated);
            Console.WriteLine("Total RDP Files Generated: " + filesGenerated.Count());
            Console.WriteLine("Select Instance Index to Launch...");
            
            while (true)
            {
                string userInput = Console.ReadLine();
                string inputNumbers = Regex.Replace(userInput, "[^0-9]", string.Empty);
                if (string.IsNullOrEmpty(inputNumbers))
                {
                    Console.WriteLine("Please enter a valid record.");
                    continue;
                }
                int clientIndex = Convert.ToInt32(inputNumbers);
                if (clientIndex < filesGenerated.Count())
                {
                    RDPClient clientToLaunch = filesGenerated[clientIndex];
                    RDP.LaunchClient(clientToLaunch);
                }
                else
                {
                    Console.WriteLine("Please enter a valid record.");
                }
            }
        }
    }
}
