﻿using Amazon.EC2.Model;
using AwsRdpGenerator.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsRdpGenerator.Business
{
    public static class RDP
    {
        public static List<RDPClient> GenerateRDPFilesForEC2Instances(List<Instance> instances)
        {
            return GenerateRDPFilesForEC2Instances(instances, string.Empty);
        }

        public static List<RDPClient> GenerateRDPFilesForEC2Instances(List<Instance> instances, InstanceKeyNameToDomainMap domainMap, RDPProperties props)
        {
            List<RDPClient> rdpClients = new List<RDPClient>();
            foreach (Instance instance in instances)
            {
                RDPClient newClient = new RDPClient();
                props.FullAddress = instance.PrivateIpAddress;
                props.Domain = domainMap.GetDomainFor(instance.KeyName);
                newClient = GenerateRDPFileForEC2Instance(instance, props);
                rdpClients.Add(newClient);
            }
            return rdpClients;
        }

        public static List<RDPClient> GenerateRDPFilesForEC2Instances(List<Instance> instances, InstanceKeyNameToDomainMap domainMap, string userName)
        {
            List<RDPClient> rdpClients = new List<RDPClient>();
            foreach (Instance instance in instances)
            {
                RDPClient newClient = new RDPClient();
                RDPProperties rdpProperties = new RDPProperties()
                {
                    FullAddress = instance.PrivateIpAddress,
                    Domain = domainMap.GetDomainFor(instance.KeyName),
                    Username = userName,
                };
                newClient = GenerateRDPFileForEC2Instance(instance, rdpProperties);
                rdpClients.Add(newClient);
            }
            return rdpClients;
        }

        public static List<RDPClient> GenerateRDPFilesForEC2Instances(List<Instance> instances, string userName)
        {
            List<RDPClient> rdpClients = new List<RDPClient>();
            foreach (Instance instance in instances)
            {
                RDPClient newClient = new RDPClient();
                RDPProperties rdpProperties = new RDPProperties()
                {
                    FullAddress = instance.PrivateIpAddress,
                    Username = userName,
                };
                newClient = GenerateRDPFileForEC2Instance(instance, rdpProperties);
                rdpClients.Add(newClient);
            }
            return rdpClients;
        }

        public static List<RDPClient> GenerateRDPFilesForEC2Instances(List<Instance> instances, string userName, string domain)
        {
            List<RDPClient> rdpClients = new List<RDPClient>();
            foreach (Instance instance in instances)
            {
                RDPClient newClient = new RDPClient();
                RDPProperties rdpProperties = new RDPProperties()
                {
                    FullAddress = instance.PrivateIpAddress,
                    Domain = domain,
                    Username = userName,
                };
                newClient = GenerateRDPFileForEC2Instance(instance, rdpProperties);
                rdpClients.Add(newClient);
            }
            return rdpClients;
        }

        public static List<RDPClient> GenerateRDPFilesForEC2Instances(List<Instance> instances, RDPProperties rdpProperties)
        {
            List<RDPClient> rdpClients = new List<RDPClient>();
            foreach (Instance instance in instances)
            {
                RDPClient newClient = new RDPClient();
                newClient = GenerateRDPFileForEC2Instance(instance, rdpProperties);
                rdpClients.Add(newClient);
            }
            return rdpClients;
        }
        
        public static RDPClient GenerateRDPFileForEC2Instance(Instance instance)
        {
            return GenerateRDPFileForEC2Instance(instance, string.Empty);
        }

        public static RDPClient GenerateRDPFileForEC2Instance(Instance instance, InstanceKeyNameToDomainMap domainMap, string userName)
        {
            string domain = domainMap.GetDomainFor(instance.KeyName);
            return GenerateRDPFileForEC2Instance(instance, userName, domain);
        }

        public static RDPClient GenerateRDPFileForEC2Instance(Instance instance, string userName)
        {
            RDPProperties rdpProperties = new RDPProperties()
            {
                FullAddress = instance.PrivateIpAddress,
                Username = userName,
            };
            return GenerateRDPFileForEC2Instance(instance, rdpProperties);
        }

        public static RDPClient GenerateRDPFileForEC2Instance(Instance instance, string userName, string domain)
        {
            RDPProperties rdpProperties = new RDPProperties()
            {
                FullAddress = instance.PrivateIpAddress,
                Domain = domain,
                Username = userName,
            };
            return GenerateRDPFileForEC2Instance(instance, rdpProperties);
        }

        public static RDPClient GenerateRDPFileForEC2Instance(Instance instance, RDPProperties rdpProperties)
        {
            RDPClient result = new RDPClient();
            result.Content = GenerateRdpFileContent(instance, rdpProperties);
            result.FileName = GenerateRdpFileName(instance);
            result.EC2Instance = instance;
            return result;
        }
        public static void WriteAllRDPFilesToDisk(List<RDPClient> clients)
        {
            string outputFolder = ConfigurationManager.AppSettings["OutputFolderPath"];
            WriteAllRDPFilesToDisk(clients, outputFolder);
        }

        public static void WriteAllRDPFilesToDisk(List<RDPClient> clients, string folderPath)
        {
            foreach (RDPClient client in clients)
            {
                client.FullPath = Path.Combine(folderPath, client.FileName);
                FileSystem.WriteStringToFile(client.Content, client.FullPath);
            }
        }

        public static void WriteRDPFileToDisk(RDPClient client)
        {
            string outputFolder = ConfigurationManager.AppSettings["OutputFolderPath"];
            WriteRDPFileToDisk(client, outputFolder);
        }

        public static void WriteRDPFileToDisk(RDPClient client, string folderPath)
        {
            client.FullPath = Path.Combine(folderPath, client.FileName);
            FileSystem.WriteStringToFile(client.Content, client.FullPath);
        }
        
        public static void LaunchClient(RDPClient clientToLaunch)
        {
            FileSystem.LaunchFile(clientToLaunch.FullPath);
        }

        private static string GenerateRdpFileContent(Instance instance, RDPProperties prop)
        {
            StringBuilder result = new StringBuilder();
            AddLineToStringBuilder(result, "full address:s:", prop.FullAddress);
            AddLineToStringBuilder(result, "alternate full address:s:", prop.AlternateFullAddress);
            AddLineToStringBuilder(result, "domain:s:", prop.Domain);
            AddLineToStringBuilder(result, "username:s:", prop.Username);
            AddLineToStringBuilder(result, "screen mode id:i:", prop.ScreenMode);
            AddLineToStringBuilder(result, "use multimon:i:", prop.MultiMon);
            AddLineToStringBuilder(result, "desktopwidth:i:", prop.DesktopWidth);
            AddLineToStringBuilder(result, "desktopheight:i:", prop.DesktopHeight);
            AddLineToStringBuilder(result, "session bpp:i:", prop.ColorDepth);
            AddLineToStringBuilder(result, "winposstr:s:", prop.WinPosStr);
            AddLineToStringBuilder(result, "compression:i:", prop.UseCompression);
            AddLineToStringBuilder(result, "keyboardhook:i:", prop.KeyboardHook);
            AddLineToStringBuilder(result, "audiocapturemode:i:", prop.AudioCaptureMode);
            AddLineToStringBuilder(result, "videoplaybackmode:i:", prop.VideoPlaybackMode);
            //result.AppendLine("connection type:i:7"); // By itself, this setting does nothing. When selected in the RDC GUI, this option changes several performance related settings (themes, animation, font smoothing, etcetera). These separate settings always overrule the connection type setting.
            AddLineToStringBuilder(result, "networkautodetect:i:", prop.NetworkAutoDetect);
            AddLineToStringBuilder(result, "bandwidthautodetect:i:", prop.BandwidthAutoDetect);
            AddLineToStringBuilder(result, "displayconnectionbar:i:", prop.DisplayConnectionBar);
            AddLineToStringBuilder(result, "disable wallpaper:i:", prop.DisplayWallpaper);
            AddLineToStringBuilder(result, "allow font smoothing:i:", prop.AllowFontSmoothing);
            AddLineToStringBuilder(result, "allow desktop composition:i:", prop.AllowDesktopComposition);
            AddLineToStringBuilder(result, "disable full window drag:i:", prop.DisableFullWindowDrag);
            AddLineToStringBuilder(result, "disable menu anims:i:", prop.DisableMenuAnims);
            AddLineToStringBuilder(result, "disable themes:i:", prop.DisableThemes);
            AddLineToStringBuilder(result, "disable cursor setting:i:", prop.DisableCursorSetting);
            AddLineToStringBuilder(result, "bitmapcachepersistenable:i:", prop.BitmapCachePersistEnable);
            AddLineToStringBuilder(result, "audiomode:i:", prop.Audiomode);
            AddLineToStringBuilder(result, "redirectprinters:i:", prop.RedirectPrinters);
            AddLineToStringBuilder(result, "redirectcomports:i:", prop.RedirectComPorts);
            AddLineToStringBuilder(result, "redirectsmartcards:i:", prop.RedirectSmartCards);
            AddLineToStringBuilder(result, "redirectclipboard:i:", prop.RedirectClipboard);
            AddLineToStringBuilder(result, "redirectposdevices:i:", prop.RedirectPosDevices);
            AddLineToStringBuilder(result, "autoreconnection enabled:i:", prop.AutoReconnect);
            AddLineToStringBuilder(result, "authentication level:i:", prop.AuthenticationLevel);
            AddLineToStringBuilder(result, "prompt for credentials:i:", prop.PromptForCredentials);
            AddLineToStringBuilder(result, "negotiate security layer:i:", prop.NegotiateSecurityLayer);
            AddLineToStringBuilder(result, "remoteapplicationmode:i:", prop.RemoteApplicationMode);
            AddLineToStringBuilder(result, "alternate shell:s:", prop.AlternateShell);
            AddLineToStringBuilder(result, "shell working directory:s:", prop.ShellWorkingDirectory);
            AddLineToStringBuilder(result, "gatewayhostname:s:", prop.GatewayHostname);
            AddLineToStringBuilder(result, "gatewayusagemethod:i:", prop.GatewayUsageMethod);
            AddLineToStringBuilder(result, "gatewaycredentialssource:i:", prop.GatewayCredentialSource);
            AddLineToStringBuilder(result, "gatewayprofileusagemethod:i:", prop.GatewayProfileUsageMethod);
            AddLineToStringBuilder(result, "promptcredentialonce:i:", prop.PromptCredentialOnce);
            AddLineToStringBuilder(result, "gatewaybrokeringtype:i:", prop.GatewayBrokeringType);
            AddLineToStringBuilder(result, "use redirection server name:i:", prop.UseRedirectionServerName);
            AddLineToStringBuilder(result, "rdgiskdcproxy:i:", prop.rdgiskdcproxy);
            AddLineToStringBuilder(result, "kdcproxyname:s:", prop.kdcproxyname);
            return result.ToString();
        }

        private static void AddLineToStringBuilder(StringBuilder builder, string rdpProp, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                builder.Append(rdpProp);
                builder.AppendLine(value);
            }
        }

        private static void AddLineToStringBuilder(StringBuilder builder, string rdpProp, int value)
        {
            AddLineToStringBuilder(builder, rdpProp, value.ToString());
        }

        private static string GenerateRdpFileName(Instance instance, string instanceTagForFilename = "Name")
        {
            string tagName = instance.Tags.Where(x => x.Key == instanceTagForFilename).FirstOrDefault().Value;
            string host = tagName ?? instance.PrivateIpAddress.Replace(".", "_") ?? instance.InstanceId;
            return host + ".rdp";
        }
    }
}
