﻿using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using AwsRdpGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsRdpGenerator.Business
{
    public class EC2
    {
        public List<Instance> GetAllEC2Instances(List<RegionEndpoint> utilisedRegionEndpoints)
        {
            List<Instance> ec2instances = new List<Instance>();
            foreach (RegionEndpoint endpoint in utilisedRegionEndpoints)
            {
                List<Instance> instancesForRegion = GetEC2InstancesByEndpoint(endpoint);
                ec2instances = ec2instances.Concat(instancesForRegion).ToList();
            }
            return ec2instances;
        }

        public List<Instance> GetEC2InstancesByEndpoint(RegionEndpoint endpoint)
        {
            List<Instance> ec2instances = new List<Instance>();
            AmazonEC2Client client = GetEC2Client(endpoint);
            var responseInstances = client.DescribeInstances();
            foreach (var reservation in responseInstances.Reservations)
            {
                ec2instances = ec2instances.Concat(reservation.Instances).ToList();
            }
            return ec2instances;
        }

        public List<Instance> GetInstancesByInstanceId(RegionEndpoint endpoint, string instanceId)
        {
            AmazonEC2Client client = GetEC2Client(endpoint);
            DescribeInstancesRequest request = new DescribeInstancesRequest();
            request.InstanceIds.Add(instanceId);
            DescribeInstancesResponse response = client.DescribeInstances(request);
            return response.Reservations.SelectMany(x => x.Instances).ToList();
        }

        public string GetConsoleScreenshotByInstanceId(RegionEndpoint endpoint, string instanceId)
        {
            AmazonEC2Client client = GetEC2Client(endpoint);
            GetConsoleScreenshotRequest request = new GetConsoleScreenshotRequest();
            request.InstanceId = instanceId;
            GetConsoleScreenshotResponse response = client.GetConsoleScreenshot(request);
            string imageContent = response.ImageData;
            return imageContent;
        }

        public static AmazonEC2Client GetEC2Client(RegionEndpoint endpoint)
        {
            return new AmazonEC2Client(AWSCredential.AccessKeyID, AWSCredential.SecretKeyID, endpoint);
        }

        public static AmazonEC2Client GetEC2Client()
        {
            return GetEC2Client(RegionEndpoint.APSoutheast2);
        }
    }
}
