﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using AwsRdpGenerator.Web.Models;
using Newtonsoft.Json;
using AwsRdpGenerator.Model;

namespace AwsRdpGenerator.Web.Core
{
    public static class PropertiesPersister
    {
        public static void SaveToDisk(RDPProperties properties)
        {
            AwsRdpService awsrdp = new AwsRdpService();
            string folder = awsrdp.GenerateRdpBaseFolderPath();
            DoCreateFolder(folder);
            string json = JsonConvert.SerializeObject(properties);
            string savePath = Path.Combine(folder, "properties.conf");
            WriteToDisk(json, savePath);
        }

        public static RDPProperties ReadFromDisk()
        {
            AwsRdpService awsrdp = new AwsRdpService();
            string folder = awsrdp.GenerateRdpBaseFolderPath();
            string filePath = Path.Combine(folder, "properties.conf");
            if (!Directory.Exists(folder) || !File.Exists(filePath))
            {
                return GetDefaultProperties();
            }
            string json;
            using (StreamReader reader = new StreamReader(filePath))
            {
                json = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<RDPProperties>(json);
        }

        private static void WriteToDisk(string content, string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.WriteLine(content);
            }
        }

        private static void DoCreateFolder(string folder)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }

        private static RDPProperties GetDefaultProperties()
        {
            RDPProperties prop = new RDPProperties();
            AwsRdpService awsrdp = new AwsRdpService();
            prop.Username = awsrdp.GetCurrentWindowsUser();
            return prop;
        }
    }
}