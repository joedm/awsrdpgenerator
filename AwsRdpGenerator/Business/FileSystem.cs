﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsRdpGenerator.Business
{
    public static class FileSystem
    {
        public static void WriteStringToFile(string content, string filePath)
        {
            string directory = Path.GetDirectoryName(filePath);
            CheckOrCreateDirectory(directory);
            RemoveExistingFile(filePath);
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.Write(content);
            }
        }
        
        /// <summary>
        /// Deletes the file if it already exists.
        /// </summary>
        /// <param name="filePath">full path to file</param>
        /// <returns>returns true if a file was found and deleted</returns>
        private static bool RemoveExistingFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if directory exists and create a new directory if it does not.
        /// </summary>
        /// <param name="directory">path of directory</param>
        /// <returns>returns true if a new directory was created</returns>
        private static bool CheckOrCreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
                return true;
            }
            return false;
        }

        internal static void LaunchFile(string filePath)
        {
            string directory = Path.GetDirectoryName(filePath);
            if (Directory.Exists(directory) && File.Exists(filePath))
            {
                System.Diagnostics.Process.Start(filePath);
            }
        }
    }
}
