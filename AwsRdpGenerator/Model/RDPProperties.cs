﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AwsRdpGenerator.Model
{
    public class RDPProperties
    {
        /// <summary>
        /// Construct the RDP Properties using default values.
        /// </summary>
        public RDPProperties()
        {
            InstantiateValues(string.Empty, string.Empty, "Administrator");
        }

        /// <summary>
        /// Construct the RDP Properties using values from your EC2 Instance.
        /// </summary>
        /// <param name="instance">EC2 Instance</param>
        public RDPProperties(string address, string domain, string user)
        {
            FullAddress = string.Empty;

        }

        /// <summary>
        /// Specifies the name or IP address (and optional port) of the remote computer that you want to connect to. No Default
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// Specifies an alternative name or IP address (and optional port) of the remote computer that you want to connect to. No Default
        /// </summary>
        public string AlternateFullAddress { get; set; }

        /// <summary>
        /// Encrypted Password password. No Default
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Specifies the name of the domain of the user. No Default
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Specifies the name of the user account that will be used to log on to the remote computer. Default: Administrator
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// 1 = start in windowed mode. 2 = start full screen. Default: 2
        /// </summary>
        public int ScreenMode { get; set; }

        /// <summary>
        /// 0 – Monitor spanning is not enabled. 1 – Monitor spanning is enabled. Default: 0
        /// </summary>
        public int MultiMon { get; set; }

        /// <summary>
        /// The width (in pixels) of the remote session desktop. Default: 1920
        /// </summary>
        public int DesktopWidth { get; set; }

        /// <summary>
        /// The height (in pixels) of the remote session desktop. Default: 1080
        /// </summary>
        public int DesktopHeight { get; set; }

        /// <summary>
        /// color depth (in bits). Default: 32
        /// </summary>
        public int ColorDepth { get; set; }

        /// <summary>
        /// Specifies the position and dimensions of the session window on the client computer. No Default
        /// </summary>
        public string WinPosStr { get; set; }

        /// <summary>
        /// 0 – Do not use bulk compression. 1 – Use bulk compression. Default: 1
        /// </summary>
        public int UseCompression { get; set; }

        /// <summary>
        /// 0 – Windows key combinations are applied on the local computer. 1 – Windows key combinations are applied on the remote computer. 2 – Windows key combinations are applied in full-screen mode only. Default: 1
        /// </summary>
        public int KeyboardHook { get; set; }

        /// <summary>
        /// 0 – Do not capture audio from the local computer. 1 – Capture audio from the local computer and send to the remote computer. Default: 0
        /// </summary>
        public int AudioCaptureMode { get; set; }

        /// <summary>
        /// 0 – Do not use RDP efficient multimedia streaming for video playback. 1 – Use RDP efficient multimedia streaming for video playback when possible. Default: 1
        /// </summary>
        public int VideoPlaybackMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int NetworkAutoDetect { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int BandwidthAutoDetect { get; set; }

        /// <summary>
        /// 0 – Do not show the connection bar. 1 – Show the connection bar. Default: 1
        /// </summary>
        public int DisplayConnectionBar { get; set; }

        /// <summary>
        /// 0 – Display the wallpaper. 1 – Do not show any wallpaper. Default: 0
        /// </summary>
        public int DisplayWallpaper { get; set; }

        /// <summary>
        /// 0 – Disable font smoothing in the remote session. 1 – Font smoothing is permitted. Default: 0
        /// </summary>
        public int AllowFontSmoothing { get; set; }

        /// <summary>
        /// 0 – Disable desktop composition in the remote session. 1 – Desktop composition is permitted. Default: 1
        /// </summary>
        public int AllowDesktopComposition { get; set; }

        /// <summary>
        /// 0 – Show the contents of the window while dragging. 1 – Show an outline of the window while dragging. Default: 1
        /// </summary>
        public int DisableFullWindowDrag { get; set; }

        /// <summary>
        /// 0 – Menu and window animation is permitted. 1 – No menu and window animation. Default: 1
        /// </summary>
        public int DisableMenuAnims { get; set; }

        /// <summary>
        /// 0 – Themes are permitted. 1 – Disable theme in the remote session. Default: 1
        /// </summary>
        public int DisableThemes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DisableCursorSetting { get; set; }

        /// <summary>
        /// 0 – Do not cache bitmaps. 1 – Cache bitmaps. Default: 1
        /// </summary>
        public int BitmapCachePersistEnable { get; set; }

        /// <summary>
        /// 0 – Play sounds on the local computer. 1 – Play sounds on the remote computer. 2 – Do not play sounds. Default: 0
        /// </summary>
        public int Audiomode { get; set; }

        /// <summary>
        /// 0 – The printers on the local computer are not available in the remote session. 1 – The printers on the local computer are available in the remote session. Default: 1
        /// </summary>
        public int RedirectPrinters { get; set; }

        /// <summary>
        /// 0 – The COM ports on the local computer are not available in the remote session. 1 – The COM ports on the local computer are available in the remote session. Default: 0
        /// </summary>
        public int RedirectComPorts { get; set; }

        /// <summary>
        /// 0 – The smart card device on the local computer is not available in the remote session. 1 – The smart card device on the local computer is available in the remote session. Default: 0
        /// </summary>
        public int RedirectSmartCards { get; set; }

        /// <summary>
        /// 0 – Do not redirect the clipboard. 1 – Redirect the clipboard. Default: 1
        /// </summary>
        public int RedirectClipboard { get; set; }

        /// <summary>
        /// 0 – The POS devices from the local computer are not available in the remote session. 1 – The POS devices from the local computer are available in the remote session. Default: 1
        /// </summary>
        public int RedirectPosDevices { get; set; }

        /// <summary>
        /// 0 – Do not attempt to reconnect. 1 – Attempt to reconnect. Default: 1
        /// </summary>
        public int AutoReconnect { get; set; }

        /// <summary>
        /// 0 – If server authentication fails, connect without giving a warning. 1 – If server authentication fails, do not connect. 2 – If server authentication fails, show a warning and allow the user to connect or not. 3 – Server authentication is not required. Default: 2
        /// </summary>
        public int AuthenticationLevel { get; set; }

        /// <summary>
        /// 0 – Remote Desktop will use the saved credentials and will not prompt for credentials. 1 – Remote Desktop will prompt for credentials. Default: 1
        /// </summary>
        public int PromptForCredentials { get; set; }

        /// <summary>
        /// 0 – Security layer negotiation is not enabled and the session is started by using Secure Sockets Layer (SSL). 1 – Security layer negotiation is enabled and the session is started by using x.224 encryption. Default: 1
        /// </summary>
        public int NegotiateSecurityLayer { get; set; }

        /// <summary>
        /// 0 – Use a normal session and do not start a RemoteApp. 1 – Connect and launch a RemoteApp. Default: 0
        /// </summary>
        public int RemoteApplicationMode { get; set; }

        /// <summary>
        /// Specifies a program to be started automatically when you connect to a remote computer. The value should be a valid path to an executable file. This setting only works when connecting to servers. No Default
        /// </summary>
        public int AlternateShell { get; set; }

        /// <summary>
        /// The working directory on the remote computer to be used if an alternate shell is specified. No Default
        /// </summary>
        public int ShellWorkingDirectory { get; set; }

        /// <summary>
        /// Specifies the hostname of the RD Gateway. No Default
        /// </summary>
        public int GatewayHostname { get; set; }

        /// <summary>
        /// 0 – Do not use an RD Gateway server. 1 – Always use an RD Gateway, even for local connections. 2 – Use the RD Gateway if a direct connection cannot be made to the remote computer (i.e. bypass for local addresses). 3 – Use the default RD Gateway settings. 4 – Do not use an RD Gateway server.
        /// Default: 4
        /// </summary>
        public int GatewayUsageMethod { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int GatewayCredentialSource { get; set; }

        /// <summary>
        /// 0 – Ask for password (NTLM). 1 – Use smart card. 4 – Allow user to select later. No Default
        /// </summary>
        public int GatewayProfileUsageMethod { get; set; }

        /// <summary>
        /// 0 – Remote Desktop will not use the same credentials . 1 – Remote Desktop will use the same credentials for both the RD gateway and the remote computer. Default 1
        /// </summary>
        public int PromptCredentialOnce { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int GatewayBrokeringType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UseRedirectionServerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string rdgiskdcproxy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string kdcproxyname { get; set; }

        private void InstantiateValues(string address, string domain, string username)
        {
            FullAddress = address;
            Domain = domain;
            Username = username;
            ScreenMode = 2;
            MultiMon = 0;
            DesktopWidth = 1920;
            DesktopHeight = 1080;
            ColorDepth = 32;
            UseCompression = 1;
            KeyboardHook = 1;
            AudioCaptureMode = 0;
            VideoPlaybackMode = 1;
            NetworkAutoDetect = 1;
            BandwidthAutoDetect = 1;
            DisplayConnectionBar = 1;
            DisplayWallpaper = 0;
            AllowFontSmoothing = 0;
            AllowDesktopComposition = 1;
            DisableFullWindowDrag = 1;
            DisableMenuAnims = 1;
            DisableThemes = 1;
            DisableCursorSetting = 1;
            BitmapCachePersistEnable = 1;
            Audiomode = 0;
            RedirectPrinters = 1;
            RedirectComPorts = 0;
            RedirectSmartCards = 0;
            RedirectClipboard = 1;
            RedirectPosDevices = 1;
            AutoReconnect = 1;
            AuthenticationLevel = 2;
            PromptForCredentials = 1;
            NegotiateSecurityLayer = 1;
            RemoteApplicationMode = 0;
            GatewayUsageMethod = 4;
            GatewayProfileUsageMethod = 1;
            PromptCredentialOnce = 1;
        }
    }
}
