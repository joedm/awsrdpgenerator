﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.EC2;
using System.Configuration;

namespace AwsRdpGenerator.Model
{
    public static class AWSCredential
    {
        private static string accessKeyID = ConfigurationManager.AppSettings["AWSAccessKeyID"];
        private static string secretKeyID = ConfigurationManager.AppSettings["AWSSecretKeyID"];

        public static string AccessKeyID { get { return accessKeyID; } }
        public static string SecretKeyID { get { return secretKeyID; } }
    }
}
