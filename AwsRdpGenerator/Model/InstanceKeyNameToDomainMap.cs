﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsRdpGenerator.Model
{
    public class InstanceKeyNameToDomainMap
    {
        private Dictionary<string, string> maps = new Dictionary<string, string>();
        public void AddOrAmendMapping(string instanceKeyName, string domainName)
        {
            if (maps.ContainsKey(instanceKeyName))
            {
                maps.Remove(instanceKeyName);
            }

            maps.Add(instanceKeyName, domainName);
        }
        public string GetDomainFor(string instanceKeyName)
        {
            string result = string.Empty; 
            maps.TryGetValue(instanceKeyName, out result);
            return result;
        }
    }
}
